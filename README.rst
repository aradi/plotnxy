PLOTNXY
=======

Minimal wrapper around Pythons matplotlib package to provide a command line
program for displaying NXY formatted data.

The utility is licensed under the *2-claused BSD-license*.

The project is hosted on `Bitbucket <https://bitbucket.org/aradi/plotnxy>`_.
